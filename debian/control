Source: python-gsd
Section: python
Priority: optional
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders: Drew Parsons <dparsons@debian.org>
Build-Depends: debhelper-compat (= 13),
 cython3,
 dh-python,
 pybuild-plugin-pyproject,
 python3-setuptools,
 python3-all-dev,
 python3-ipython,
 python3-numpy (>= 1:1.9.3~), python3-numpy (<< 1:2),
 python3-sphinx
Build-Depends-Indep: python3-doc, python-numpy-doc,
 libjs-mathjax,
 python3-sphinx-rtd-theme
Standards-Version: 4.6.1
Homepage: https://gsd.readthedocs.io/
Vcs-Browser: https://salsa.debian.org/debichem-team/python-gsd
Vcs-Git: https://salsa.debian.org/debichem-team/python-gsd.git
Rules-Requires-Root: no

Package: python3-gsd
Architecture: any
Depends: ${python3:Depends}, ${shlibs:Depends}, ${misc:Depends}
Suggests: python-gsd-doc
Description: native file format for HOOMD-blue (Python 3 module)
 The GSD file format is the native file format for HOOMD-blue. GSD
 files store trajectories of the HOOMD-blue system state in a binary
 file with efficient random access to frames. GSD allows all particle
 and topology properties to vary from one frame to the next. Use the
 GSD Python API to specify the initial condition for a HOOMD-blue
 simulation or analyze trajectory output with a script. Read a GSD
 trajectory with a visualization tool to explore the behavior of the
 simulation.
 .
 This package installs the library to access GSD files from Python 3.

Package: python-gsd-doc
Architecture: all
Section: doc
Depends: libjs-mathjax, ${sphinxdoc:Depends}, ${misc:Depends}
Description: native file format for HOOMD-blue (documentation)
 The GSD file format is the native file format for HOOMD-blue. GSD
 files store trajectories of the HOOMD-blue system state in a binary
 file with efficient random access to frames. GSD allows all particle
 and topology properties to vary from one frame to the next. Use the
 GSD Python API to specify the initial condition for a HOOMD-blue
 simulation or analyze trajectory output with a script. Read a GSD
 trajectory with a visualization tool to explore the behavior of the
 simulation.
 .
 This is the common documentation package.
